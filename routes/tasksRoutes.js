
let express = require('express');
let router = express.Router();
let User = require('./../models/User');


//insert a new user
router.post("/add-task", (req, res)=>{
	// console.log(req.body);
	let newUser = new User({
				firstName: req.body.firstName,
				lastName: req.body.lastName,
				userName: req.body.userName,
				password: req.body.password
			})

	newUser.save( (savedUser, error) => {
		if(error){
			res.send(error)
		} else {
			res.send(`New user saved:`, savedUser);
		}
	})
});

//retrieve all users
router.get("/users", (req, res)=>{

	//model.method
		//search users from the database
	User.find({}, (result, error)=>{

		if(error){
			res.send(error)
		} else {

			//send it as a response
			res.send(result);
				//return an array of documents
		}
	})
});

//retrieve a specific user
router.get("/users/:id", (req, res)=>{
	// console.log(req.params);
	let params = req.params.id

	//model.method
	User.findById(params, (result, error)=>{
		if(error){
			res.send(error)
		} else {
			//send document as a response
			res.send(result);
		}
	})
})

//update user's info


module.exports = router;
7